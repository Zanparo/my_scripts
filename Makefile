##
## Makefile for sponge in /home/darrac_s/rendu/piscine_cpp_d01/ex01
## 
## Made by Samuel Darracq
## Login   <darrac_s@epitech.net>
## 
## Started on  Wed Jan  7 10:47:25 2015 Samuel Darracq
## Last update Sat Jan 17 09:49:24 2015 Samuel Darracq
##

NAME	=	

CFLAGS	+=	-Wall -Werror -Wextra
CFLAGS	+=	-I ./includes/

SRCS	=	main.cpp		\


OBJS	=	$(SRCS:.c=.o)

CC	=	g++ 

RM	=	rm -rf

all	:	$(NAME)

$(NAME)	:	$(OBJS)
		$(CC) -o $(NAME) $(OBJS) $(CFLAGS)

clean	:	
		$(RM) $(OBJS)

fclean	:	clean
		$(RM) $(NAME)

re	:	fclean all

dbg	:	CGLAGS+=-g
